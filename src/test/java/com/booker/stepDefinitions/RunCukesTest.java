package com.booker.stepDefinitions;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
				plugin={"pretty", 
						"html:target/cucumber-html-report", 
						"json:target/cucumber-json-report.json", 
						"usage:target/cucumber-usage.json", 
						"junit:target/cucumber-junit-report/cucumber-results.xml"},
				features={"src/test/resources/"},		  
				glue={"com/booker/stepDefinitions"},
				tags={"@testChallenge"}
				)
public class RunCukesTest {

}

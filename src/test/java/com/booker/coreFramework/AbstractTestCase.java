package com.booker.coreFramework;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

import com.booker.configuration.Context;
import com.booker.pageObjects.LoginPage;
import com.booker.coreFramework.WebDriverFactory;

@ContextConfiguration(classes = Context.class, initializers = ConfigFileApplicationContextInitializer.class )
public abstract class AbstractTestCase {
	
	protected WebDriver driver;
	
	@Value("${bookerUrl}")
	private String bookerUrl;
	
	public void printUrl(){
		System.out.println(bookerUrl);
	}
	
	@Autowired
	protected RestTemplate restTemplate;
	
	public void printRestTemplate(){
		System.out.println(restTemplate);
	}
	
	@Autowired
	private WebDriverFactory webDriverFactory;
	
	public void printWebDriverFactory(){
		System.out.println(webDriverFactory);
	}
	
	public String navigateTobookerUrl(){
		return bookerUrl;
	}

	protected LoginPage goToLoginPage(){
		driver = webDriverFactory.initializeWebDriver();
		driver.get(navigateTobookerUrl());
		System.out.println(driver.getTitle());
	return new LoginPage(driver);
	}
}

package com.booker.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.booker.coreFramework.AbstractWebPage;

public class LandingPage extends AbstractWebPage{

	
	@FindBy(how = How.CSS, using = "#hotelName")
	private WebElement enterHotelName;
	
	@FindBy(how = How.CSS, using = "#address")
	private WebElement enterAddress;
	
	@FindBy(how = How.CSS, using = "#owner")
	private WebElement enterOwner;
	
	@FindBy(how = How.CSS, using = "#phone")
	private WebElement enterPhoneNo;
	
	@FindBy(how = How.CSS, using = "#email")
	private WebElement enterEmail;
	
	@FindBy(how = How.ID, using = "createHotel")
	private WebElement createEntry;
	
	@FindBy(how = How.CSS, using = ".row.detail div:nth-child(2) span")
	private WebElement deleteEntry;
	
	@FindBy(how = How.LINK_TEXT, using = "Home")
	private WebElement homeLink;
	
	@FindBy(how = How.CSS, using = "#logout")
	private WebElement logOut;
	
	
	public LandingPage(WebDriver driver) {
		super(driver);
	}
	
	public void provideEntries(String HotelName, String Address, String Owner, String PhoneNo, String Email){
		enterHotelName.clear();
		enterHotelName.sendKeys(HotelName);
		
		enterAddress.clear();
		enterAddress.sendKeys(Address);
		
		enterOwner.clear();
		enterOwner.sendKeys(Owner);
		
		enterPhoneNo.clear();
		enterPhoneNo.sendKeys(PhoneNo);
		
		enterEmail.clear();
		enterEmail.sendKeys(Email);
		
	}
	
	public void createAnEntry(){
		createEntry.click();
	}
	
	public void deleteEntry(){
		deleteEntry.click();
	}
	
	public void logOffAdmin(){
		logOut.click();
	}
	
	public void clearInputFields(){
		enterHotelName.clear();
		enterAddress.clear();
		enterOwner.clear();
		enterPhoneNo.clear();
		enterEmail.clear();
	}
	
	public void navigateToHome(){
		homeLink.click();
	}
	
	public void closeBrowser(){
		driver.close();
		driver.quit();
	}
}
